﻿using MotorAutomobil.Models;
using MotorAutomobil.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MotorAutomobil.Controllers
{
    public class AutomobilController : ApiController
    {
        

        IAutomobilRepository _repository { get; set; }

        public AutomobilController()
        {
            _repository = new AutomobilRepository();
        }

        public AutomobilController(IAutomobilRepository repository)
        {
            _repository = repository;
        }


        //Get: api/automobil
        public IQueryable<Automobil> GetAutomobils()
        {
            
            return _repository.GetAll();

        }

        [ResponseType(typeof(Automobil))]
        public IHttpActionResult GetAutomobil(int id)
        {
            var automobil = _repository.GetById(id);
            if (automobil == null)
            {
                return NotFound();
            }
            return Ok(automobil);
        }

        [Route("api/DvaNajEkonomicnijaProizvoda")]
        
        public IQueryable<Automobil> GetTwoMostEconomicAutomobils()
        {
            return _repository.GetTwoMostEconomicAutomobils();
        }

        //Post api/automobil
        [ResponseType(typeof(Automobil))]
        public IHttpActionResult PostAutomobil(Automobil automobil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(automobil);

            return CreatedAtRoute("DefaultApi", new { id = automobil.Id }, automobil);
        }


        //Delete api/automobil/1
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteAutomobil(int id)
        {
            var automobil = _repository.GetById(id);
            if (automobil == null)
            {
                return NotFound();
            }

            _repository.Delete(automobil);
            return Ok();
        }

        //Put api/automobil/1
        [ResponseType(typeof(Automobil))]
        public IHttpActionResult PutAutomobil(int id, Automobil automobil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != automobil.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(automobil);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(automobil);
        }
    }
}
