﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotorAutomobil.Helpers
{
    public class AutomobilResourceParameters:PaginationResourceParameters
    {
        public string Name { get; set; }
    }
}