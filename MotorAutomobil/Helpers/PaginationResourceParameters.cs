﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotorAutomobil.Helpers
{
    public abstract class PaginationResourceParameters
    {
        private const int MaxPagesize = 20;
        public int PageNumber { get; set; } = 1;
        private int pageSize = 10;
        public int PgeSize
        {
            get => pageSize;
            set => pageSize=(value>MaxPagesize)?MaxPagesize:value;
        }
    }
}