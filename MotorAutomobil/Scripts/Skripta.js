﻿$(document).ready(function () {

    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var editingId;

    var formAction = "Create";

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");

    $("#automobil").css("display", "none");//sakriva  dugme ucitaj automobile dok se ne ulogujes

    //CRUD operacije: D - delete (klik na button sa id-om "btnDelete")
    $("body").on("click", "#btnDelete", deleteAutomobil);

    //CRUD operacije: U - update (klik na button sa id-om "btnEdit")
    $("body").on("click", "#btnEdit", editAutomobil);

    
    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };


        $.ajax({
            type: "POST",
            url: "http://" + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        }).fail(function (data) {
            alert(data);
        });


    });


    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");

            $("#automobil").css("display", "block");//blokira sakrivajucu funkciju za ucitavanje automobila

            $(".container2").css("visibility", "visible");
            $(".editDelete").css("visibility", "visible");

        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};

        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#info").empty();
        $("#sadrzaj").empty();

        $(".container2").css("visibility", "hidden");
        $(".editDelete").css("visibility", "hidden");


    })

    //CRUD operacije: C - create (klik na button sa id-om "unesti")
    $("#automobilForm").submit(function (e) {

        e.preventDefault();

        var automobilProizvodjac = $("#automobilProizvodjac").val();
        var automobilModel = $("#automobilModel").val();
        var automobilGodinaProizvodnje = $("#automobilGodinaProizvodnje").val();
        var automobilKubikaza = $("#automobilKubikaza").val();
        var automobilBoja = $("#automobilBoja").val();

        var selectedId = $("#abc option:selected").val();

        //if (token) {
        //    headers.Authorization = "Bearer" + token;
        //}

        var httpAction;
        var sendData;
        var url;

        if (formAction == "Create") {
            httpAction = "POST";
            url = "http://" + host + "/api/automobil/"
            sendData = {
                "Proizvodjac": automobilProizvodjac,
                "Model": automobilModel,
                "GodinaProizvodnje": automobilGodinaProizvodnje,
                "Kubikaza": automobilKubikaza,
                "Boja": automobilBoja,
                "MotorId": selectedId
            };
        }
        else {
            httpAction = "PUT";
            url = "http://" + host + "/api/automobil/" + editingId;
            sendData = {
                "Id": editingId,
                "Proizvodjac": automobilProizvodjac,
                "Model": automobilModel,
                "GodinaProizvodnje": automobilGodinaProizvodnje,
                "Kubikaza": automobilKubikaza,
                "Boja": automobilBoja,
                "MotorId": selectedId
            };
        }

        $.ajax({
            type: httpAction,
            url: url,
            data: sendData,
            headers: headers
        }).done(function (data) {
            formAction = "Create";
            refreshTable();
            formiranjeTabele();//dodatak
        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    })



    function deleteAutomobil() {

        //if (token) {
        //    headers.Authorization = "Bearer" + token;
        //}

        // izvlacimo {id}
        var deleteID = this.name;
        // saljemo zahtev 
        $.ajax({
            type: "DELETE",
            url: "http://" + host + "/api/automobil/" + deleteID,
            headers: headers
        }).done(function (data, status) {
            alert("Uspesno ste izbrisali proizvod sa id-om " + deleteID);
            refreshTable();
        })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });

    }

    function editAutomobil() {

        var editId = this.name;
        // saljemo zahtev da dobavimo taj auto
        $.ajax({
            url: "http://" + host + "/api/automobil/" + editId,
            type: "GET",
        })
            .done(function (data, status) {
                $("#automobilProizvodjac").val(data.Proizvodjac);
                $("#automobilModel").val(data.Model);
                $("#automobilGodinaProizvodnje").val(data.GodinaProizvodnje);
                $("#automobilKubikaza").val(data.Kubikaza);
                $("#automobilBoja").val(data.Boja);

                $("#abc").val(data.MotorId);
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Desila se greska!");
            });

    };

    function formiranjeTabele(data, status) {
        console.log("Status: " + status);

        var $container = $("#data");
        $container.empty();

        if (status == "success") {
            //console.log(data);
            // ispis naslova
            var div = $("<div></div>");
            var h1 = $("<h1>Automobil</h1>");
            div.append(h1);
            // ispis tabele
            var table = $("<table class=table  ></table>");
            var header = $("<tr> <td>Id</td> <td>Proizvodjac</td> <td>Model</td><td>Godina proizvodnje</td> <td>Kubikaza</td> <td>Boja</td> <td>Motor</td> <td class=editDelete>Delete</td> <td class=editDelete>Edit</td> </tr>");
            table.append(header);
            for (i = 0; i < data.length; i++) {
                // prikazujemo novi red u tabeli
                var row = "<tr>";
                // prikaz podataka
                var displayData = "<td>" + data[i].Id + "</td> <td>" + data[i].Proizvodjac + "</td> <td>" + data[i].Model + "</td> <td>" + data[i].GodinaProizvodnje + "</td> <td>" + data[i].Kubikaza + "</td> <td>" + data[i].Boja + "</td> <td>" + data[i].Motor.Naziv + "</td>";
                // prikaz dugmadi za izmenu i brisanje
                var stringId = data[i].Id.toString();
                var displayDelete = "<td><button id=btnDelete name=" + stringId + " class=editDelete>Delete</button></td>";
                var displayEdit = "<td><button id=btnEdit name=" + stringId + " class=editDelete>Edit</button></td>";
                row += displayData + displayDelete + displayEdit + "</tr>";
                table.append(row);
                //newId = data[i].Id;
            }

            div.append(table);

            formiranjeDropDownListe();

            // prikaz forme
            $("#formDiv").css("display", "block");

            // ispis novog sadrzaja
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Greška prilikom preuzimanja automobila!</h1>");
            div.append(h1);
            $container.append(div);
        }
    }

    function formiranjeDropDownListe() {
        //var requestUrl = host + port + motorEndpoint;
        var requestUrl = "http://" + host + "/api/motor";

        $.getJSON(requestUrl, function (data, status) {

            $("#abc").remove();

            if (status == "success") {

                var motor = $("<select id='abc'></select>");

                for (var i = 0; i < data.length; i++) {
                    var row = "<option value='" + data[i].Id + "'>" + data[i].Naziv + "</option>";
                    motor.append(row);
                }
            }

            $(motor).appendTo("#listaMotoraId");
        })
    }

    //CRUD operacije: R - read (klik na button sa id-om "proizvodi")
    $("#automobil").click(function () {
        // korisnik mora biti ulogovan
        //if (token) {
        //    headers.Authorization = "Bearer" + token;
        //}

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/automobil/",
            //"headers": headers

        }).done(function (data, status) {
            //formiranje tabele OVDEEEEE!!!!
            formiranjeTabele(data, status);

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    });

    function refreshTable() {
        // cistim formu
        $("#automobilProizvodjac").val('');
        $("#automobilModel").val('');
        $("#automobilGodinaProizvodnje").val('');
        $("#automobilKubikaza").val('');
        $("#automobilBoja").val('');
       
        // osvezavam
        $("#btnAutomobil").trigger("click");

        $("#abc").remove();
    };

});