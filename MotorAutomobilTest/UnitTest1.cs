﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MotorAutomobil.Controllers;
using MotorAutomobil.Models;
using MotorAutomobil.Repository;

namespace MotorAutomobilTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Automobil> products = new List<Automobil>();
            products.Add(new Automobil { Id = 1, Proizvodjac = "Proizvodjac1", Model="307", Kubikaza = 100, GodinaProizvodnje=2005, Boja="plava", MotorId = 1 });
            products.Add(new Automobil { Id = 2, Proizvodjac = "Proizvodjac2", Model = "307", Kubikaza = 200, GodinaProizvodnje = 2005, Boja = "plava", MotorId = 2 });

            var mockRepository = new Mock<IAutomobilRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            var controller = new AutomobilController(mockRepository.Object);

            // Act
            IEnumerable<Automobil> result = controller.GetAutomobils();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(products.Count, result.ToList().Count);
            Assert.AreEqual(products.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(products.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void GetTwoMostEconomicAutomobils()
        {
            // Arrange
            List<Automobil> automobils = new List<Automobil>();
            automobils.Add(new Automobil { Id = 1, Proizvodjac = "Proizvodjac1", Model = "307", Kubikaza = 50, GodinaProizvodnje = 2005, Boja = "plava", MotorId = 1 });
            automobils.Add(new Automobil { Id = 2, Proizvodjac = "Proizvodjac2", Model = "307", Kubikaza = 100, GodinaProizvodnje = 2005, Boja = "plava", MotorId = 2 });

            var mockRepository = new Mock<IAutomobilRepository>();
            mockRepository.Setup(x => x.GetTwoMostEconomicAutomobils()).Returns(automobils.AsQueryable());
            var controller = new AutomobilController(mockRepository.Object);

            // Act
            IEnumerable<Automobil> result = controller.GetTwoMostEconomicAutomobils();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.ToList().Count);
            Assert.AreEqual(automobils.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(automobils.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void GetReturnsAutomobilWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IAutomobilRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Automobil { Id = 42 });

            var controller = new AutomobilController(mockRepository.Object);

            //// Act
            IHttpActionResult actionResult = controller.GetAutomobil(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Automobil>;

            //// Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IAutomobilRepository>();
            var controller = new AutomobilController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetAutomobil(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IAutomobilRepository>();
            var controller = new AutomobilController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.PostAutomobil(new Automobil { Id = 10, Proizvodjac = "Proizvodjac1", Model = "307", Kubikaza = 50, GodinaProizvodnje = 2005, Boja = "plava", MotorId = 1 });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Automobil>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IAutomobilRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Automobil { Id = 10 });
            var controller = new AutomobilController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.DeleteAutomobil(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IAutomobilRepository>();
            var controller = new AutomobilController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.PutAutomobil(10, new Automobil { Id = 9, Proizvodjac = "Skoda" });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }
    }
}
