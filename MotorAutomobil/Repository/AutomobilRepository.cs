﻿using MotorAutomobil.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace MotorAutomobil.Repository
{
    public class AutomobilRepository:IDisposable,IAutomobilRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable<Automobil> GetAll()
        {
            //return db.Proizvods.Include(x => x.Kategorija).OrderBy(x => x.Cena);

            return db.Automobils.Include(x => x.Motor).OrderBy(x => x.Kubikaza);
        }

        public Automobil GetById(int id)
        {
            Automobil automobil = db.Automobils.Find(id);
            return automobil;
        }

        public IQueryable<Automobil> GetTwoMostEconomicAutomobils()
        {
            return db.Automobils.OrderBy(x => x.Kubikaza).Take(2);
        }

        public void Add(Automobil automobil)
        {
          
           
            db.Automobils.Add(automobil);
            db.SaveChanges();
            
        }

        public void Delete(Automobil automobil)
        {
            db.Automobils.Remove(automobil);
            db.SaveChanges();
        }

        public void Update(Automobil automobil)
        {
            db.Entry(automobil).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        private bool AutomobilExists(int id)
        {
            return db.Automobils.Count(e => e.Id == id) > 0;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}