﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MotorAutomobil.Models
{
    public class Automobil
    {
        public int Id { get; set; }
      
        public string Proizvodjac { get; set; }
        
        public string Model { get; set; }
       
        [Range(1980,int.MaxValue)]
        public int GodinaProizvodnje { get; set; }
      
        [Range(0,float.MaxValue)]
        public float Kubikaza { get; set; }
        
        public string Boja { get; set; }

        public int MotorId { get; set; }
      
        public Motor Motor { get; set; }
    }
}