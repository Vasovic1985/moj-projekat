﻿using MotorAutomobil.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MotorAutomobil.Controllers
{
    public class MotorController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //Get: api/motor
        public IQueryable<Motor> GetMotor()
        {
            return db.Motors;
        }



        //Get api/motor/1
        [ResponseType(typeof(Motor))]
        public IHttpActionResult GetMotor(int id)
        {
            Motor motor = db.Motors.Find(id);
            if (motor == null)
            {
                return NotFound();
            }
            return Ok(motor);
        }

        //Post api/motor
        [ResponseType(typeof(Motor))]
        public IHttpActionResult PostMotor(Motor motor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Motors.Add(motor);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = motor.Id }, motor);
        }
        private bool MotorExists(int id)
        {
            return db.Motors.Count(e => e.Id == id) > 0;
        }
        //Put api/motor/1
        [ResponseType(typeof(Motor))]
        public IHttpActionResult PutMotor(int id, Motor motor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != motor.Id)
            {
                return BadRequest();
            }
            db.Entry(motor).State = EntityState.Modified;
            try
            {

                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MotorExists(id))
                {

                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(motor);
        }

        //Delete api/motor/1
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteMotor(int id)
        {
            Motor motor = db.Motors.Find(id);
            if (motor == null)
            {

                return NotFound();
            }
            db.Motors.Remove(motor);
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
