namespace MotorAutomobil.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracija6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Automobils", "Proizvodjac", c => c.String());
            AlterColumn("dbo.Automobils", "Model", c => c.String());
            AlterColumn("dbo.Automobils", "Boja", c => c.String());
            AlterColumn("dbo.Motors", "Naziv", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Motors", "Naziv", c => c.String(nullable: false));
            AlterColumn("dbo.Automobils", "Boja", c => c.String(nullable: false));
            AlterColumn("dbo.Automobils", "Model", c => c.String(nullable: false));
            AlterColumn("dbo.Automobils", "Proizvodjac", c => c.String(nullable: false));
        }
    }
}
