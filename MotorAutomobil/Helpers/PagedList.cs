﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotorAutomobil.Helpers
{
    public class PagedList<T>:List<T>
    {
        public int CurentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }

        private bool HasPrevious
        {

            get
            {
                return (CurentPage > 1);
            }
        }
        private bool HasNext
        {
            get
            {
                return (CurentPage < TotalPages);
            }
        }
        public PagedList(List<T> items,int TotalCount,int PageNumber,int PageSize)
        {
            this.TotalCount = TotalCount;
            this.PageSize = PageSize;
            this.CurentPage = PageNumber;
            TotalPages = (int)Math.Ceiling(TotalCount / (double)PageSize);
            AddRange(items);
        }
        public static PagedList<T> Create(IQueryable<T> source,int PageNumber,int PageSize)
        {

            var TotalCount = source.Count();
            var items = source.Skip((PageNumber-1)*PageSize).Take(PageSize).ToList();
            return new PagedList<T>(items, TotalCount, PageNumber, PageSize);

        }
    }
}