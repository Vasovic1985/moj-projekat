﻿using MotorAutomobil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotorAutomobil.Repository
{
    public interface IAutomobilRepository
    {


        IQueryable<Automobil> GetAll();
        Automobil GetById(int id);
        IQueryable<Automobil> GetTwoMostEconomicAutomobils();
        void Add(Automobil automobil);
        void Update(Automobil automobil);
        void Delete(Automobil id);
    }
}
